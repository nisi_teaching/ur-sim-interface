import RPi.GPIO as gpio
import time
import socket
import ur


HOST = '192.168.0.164' # The remote host
PORT1 = 30001
PORT2 = 30002
continueRunning = True

# Thread S1 for read
s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create socket for TCP/IP
s1.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # Setup socket to be able to reconnect if program is crashed
s1.connect((HOST, PORT1)) # Connect socket to remote host

# Thread S2 for send
s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
s2.connect((HOST, PORT2)) # Connect socket to remote host


ur_state = ur.UR_RobotState(s1) 
ur_state.start() 

gpio.setmode(gpio.BCM)
gpio.setup(17, gpio.OUT)
gpio.setup(27, gpio.OUT)


oldValueD0 = -1
oldValueD1 = -1

while continueRunning:

    di_array = ur_state.get_digital_output()
    #print(di_array[0]) 
    
    
    # Check digital output and set digital tool output 0
    if di_array[0][0] == 0:
      if oldValueD0 != di_array[0][0]:
          oldValueD0 = di_array[0][0]
          gpio.output(17, gpio.LOW)
          s2.send(b'set_tool_digital_out(0,False)' + b'\n')
          print("D0 = 0 - Write" )
    else:
      #print("D0 = 1 - READ")
      if oldValueD0 != di_array[0][0]:
          oldValueD0 = di_array[0][0]
          gpio.output(17, gpio.HIGH)
          s2.send(b'set_tool_digital_out(0,True)' + b'\n')
          print("D0 = 1 - Write" )

    # Check digital output and set digital tool output 1
    if di_array[0][1] == 0:
      if oldValueD1 != di_array[0][1]:
          oldValueD1 = di_array[0][1]
          gpio.output(27, gpio.LOW)
          s2.send(b'set_tool_digital_out(1,False)' + b'\n')
          print("D1 = 0 - Write" )
    else:
      if oldValueD1 != di_array[0][1]:
          oldValueD1 = di_array[0][1]
          gpio.output(27, gpio.HIGH)
          s2.send(b'set_tool_digital_out(1,True)' + b'\n')
          print("D1 = 1 - Write" )

    # Stop if digital output 7 is True
    if di_array[0][7] == 1:
        print("D7 = 1 - Stop" )
        continueRunning = False
      
      


gpio.cleanup()
ur_state.stop() # Stop the ur_state thread
s1.close() # Close the sock
s2.close()





